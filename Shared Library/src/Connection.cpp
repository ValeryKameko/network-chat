#include "Connection.h"
#include <WS2tcpip.h>


Connection::Connection(SOCKET socket)
    : socket(socket)
{
    connectionStream.reset(new ConnectionStream(this));
    IsEstablished = true;
}

Connection::~Connection()
{
}

std::shared_ptr<ConnectionStream> Connection::GetStream()
{
    return connectionStream;
}

SOCKADDR_IN Connection::GetHostAddress(SOCKET socket)
{
    SOCKADDR_IN address;
    int size = sizeof(address);
    getsockname(socket, (SOCKADDR *)&address, &size);
    return address;
}

SOCKADDR_IN Connection::GetPeerAddress(SOCKET socket)
{
    SOCKADDR_IN address;
    int size = sizeof(address);
    getpeername(socket, (SOCKADDR *)&address, &size);
    return address;
}

int Connection::GetHostPort()
{
    return ntohs(GetHostAddress(socket).sin_port);
}

IN_ADDR Connection::GetHostIP()
{
    return GetHostAddress(socket).sin_addr;
}

std::string Connection::GetHostIPString()
{
    constexpr size_t BUFFER_SIZE = 60;
    char buffer[BUFFER_SIZE];
    IN_ADDR addr = GetHostIP();
    return std::string(inet_ntop(AF_INET, &addr, buffer, BUFFER_SIZE));
}


int Connection::GetPeerPort()
{
    return ntohs(GetPeerAddress(socket).sin_port);
}

IN_ADDR Connection::GetPeerIP()
{
    return GetPeerAddress(socket).sin_addr;
}

std::string Connection::GetPeerIPString()
{
    constexpr size_t BUFFER_SIZE = 60;
    char buffer[BUFFER_SIZE];
    IN_ADDR addr = GetPeerIP();
    return std::string(inet_ntop(AF_INET, &addr, buffer, BUFFER_SIZE));
}

void Connection::AddCloseConnectionCallback(const CloseConnectionCallback &callback)
{
    onCloseConnection.push_back(callback);
}

void Connection::Close()
{
    if (IsEstablished) {
        IsEstablished = false;
        for (auto &callback : onCloseConnection)
            callback();
        closesocket(socket);
    }
}
