#include "MessageStream.h"
#include <sstream>
#include <iomanip>


MessageStream::MessageStream(const std::shared_ptr<ConnectionStream> & stream)
    : stream(stream)
{
}


MessageStream::~MessageStream()
{
}

void MessageStream::Send(const Message & message)
{
    ByteData data = message.Serialize();
    stream->Send(data);
}

std::shared_ptr<Message> MessageStream::Receive()
{
    std::shared_ptr<Message> message(new Message());

    size_t it = 0;
    bool valid = true;
    ByteData messageNameData;
    std::string messageName;

    valid = valid && ReceiveHeaderEntry(messageNameData);
    valid = valid && ParseMessageName(messageNameData, messageName);
    valid = valid && Message::MessageTypesByName.find(messageName) != Message::MessageTypesByName.end();
    if (valid)
        message->SetMessageType(Message::MessageTypesByName.at(messageName));

    while (valid) {
        ByteData headerEntryData;
        valid = valid && ReceiveHeaderEntry(headerEntryData);
        if (valid && CheckHeadersEnd(headerEntryData))
            break;
        std::string header, value;
        valid = valid && ParseHeaderEntry(headerEntryData, header, value);
        if (valid)
            message->SetHeader(header, value);
    }
    int64_t dataLength;
    valid = valid && message->HeaderInt64Value("DataLength", dataLength);
    valid = valid && dataLength >= 0 && dataLength < Message::MaxMessageSize;
    if (valid) {
        ByteData messageData;
        stream->Receive((size_t)dataLength, messageData);
        message->SetData(messageData);
    }
    if (valid)
        return message;
    else
        return nullptr;
}

char MessageStream::GetByte(size_t & it, const ByteData & data)
{
    char byte = data.GetByte(it);
    return byte;
}

bool MessageStream::GetSpecificBytes(size_t & it, const ByteData & data, const char * str, size_t count)
{
    bool valid = true;
    for (size_t i = 0; i < count; i++)
        valid = valid && GetSpecificByte(it, data, str[i]);
    return valid;
}

bool MessageStream::GetSpecificBytes(size_t & it, const ByteData & data, const char * str)
{
    bool valid = true;
    for (const char * c = str; *c != '\0'; c++)
        valid = valid && GetSpecificByte(it, data, *c);
    return valid;
}


bool MessageStream::GetSpecificByte(size_t & it, const ByteData & data, char c)
{
    if (it == data.GetSize() || GetByte(it, data) != c)
        return false;
    it++;
    return true;
}

bool MessageStream::GetByteOneOf(size_t & it, const ByteData & data, const std::string & allowed, char & readByte)
{
    if (it == data.GetSize())
        return false;
    readByte = GetByte(it, data);
    if (allowed.find(readByte) == std::string::npos)
        return false;
    it++;
    return true;
}

bool MessageStream::GetByteNotOneOf(size_t & it, const ByteData & data, const std::string & allowed, char & readByte)
{
    if (it == data.GetSize())
        return false;
    readByte = GetByte(it, data);
    if (allowed.find(readByte) != std::string::npos)
        return false;
    it++;
    return true;
}

bool MessageStream::ReceiveHeaderEntry(ByteData & data)
{
    data = ByteData();
    ByteData readByte;
    while (true) {
        stream->Receive(1, readByte);
        if (readByte.GetSize() != 1)
            return false;
        data += readByte;
        if (readByte.GetByte(0) == '\n')
            break;
    }
    return true;
}

bool MessageStream::ParseMessageName(const ByteData & data, std::string & messageName)
{
    size_t it = 0;
    bool valid = true;
    char readByte;

    while (GetByteOneOf(it, data, Message::MetaCharsAllowed, readByte))
        messageName += readByte;

    valid = valid && GetSpecificBytes(it, data, "\r\n");
    valid = valid && it == data.GetSize();
    return valid;
}

bool MessageStream::ParseHeaderEntry(const ByteData & data, std::string & header, std::string & value)
{
    header = "";
    value = "";
    size_t it = 0;
    bool valid = true;
    char readByte;

    while (GetByteOneOf(it, data, Message::MetaCharsAllowed, readByte))
        header += readByte;
    valid = valid && GetSpecificBytes(it, data, ": ");
    while (GetByteNotOneOf(it, data, "\r\n", readByte))
        value += readByte;
    valid = valid && GetSpecificBytes(it, data, "\r\n");
    valid = valid && it == data.GetSize();
    return valid;
}

bool MessageStream::CheckHeadersEnd(const ByteData & data)
{
    bool valid = true;
    size_t it = 0;
    valid = valid && GetSpecificBytes(it, data, "\r\n");
    valid = valid && it == data.GetSize();
    return valid;
}
