#include "ConnectionStream.h"
#include <stdlib.h>
#include <WinSock2.h>

ConnectionStream::ConnectionStream(Connection * connection)
    : connection(connection)
{
}


ConnectionStream::~ConnectionStream()
{

}

int ConnectionStream::Send(const ByteData & data)
{
    int iResult;
    iResult = ::send(connection->socket, data.GetBytes(), data.GetSize(), 0);
    if (iResult <= 0) {
        connection->Close();
        return -1;
    }
    return iResult;
}

int ConnectionStream::Receive(size_t size, ByteData & data) 
{
    data = ByteData();

    char *buffer = (char *)malloc(size);

    while (data.GetSize() < size) {
        int iResult;
        iResult = ::recv(connection->socket, buffer, size, 0);

        if (iResult <= 0) {
            connection->Close();
            return -1;
        }

        data += ByteData(buffer, iResult);
    }

    free(buffer);

    return 0;
}

Connection * ConnectionStream::GetConnection() const
{
    return connection;
}
