#include "WinThread.h"



WinThread::WinThread()
{
    hThread = CreateThread(NULL, 0, &WinThread::ThreadProc, this, CREATE_SUSPENDED, &ThreadId);
}


WinThread::~WinThread()
{
}

void WinThread::Start()
{
    Resume();
}

DWORD WinThread::GetId()
{
    return ThreadId;
}

void WinThread::Suspend()
{
    SuspendThread(hThread);
}

void WinThread::Resume()
{
    ResumeThread(hThread);
}

void WinThread::Terminate(DWORD dwCode)
{
    OnInternalExit(dwCode);
    TerminateThread(hThread, dwCode);
}

void WinThread::Wait()
{
    WaitForSingleObject(hThread, INFINITE);
}

void WinThread::Exit(DWORD dwCode)
{
    OnInternalExit(dwCode);
    ExitThread(dwCode);
}

void WinThread::OnExit()
{
}

DWORD WinThread::ThreadProc(LPVOID lpParameter)
{
    WinThread * self = reinterpret_cast<WinThread *>(lpParameter);
    self->Run();

    self->Exit(0);
    return 0;
}

void WinThread::OnInternalExit(DWORD dwCode)
{
    OnExit();
    delete this;
}
