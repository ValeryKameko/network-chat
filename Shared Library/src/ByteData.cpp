#include "ByteData.h"
#include <windows.h>

ByteData::ByteData(const std::vector<char> & bytes)
    : ByteData(bytes.data(), bytes.size())
{
}

ByteData::ByteData(const std::string & str)
    : ByteData(str.data(), str.size())
{

}

ByteData::ByteData(const char * const bytes, size_t size)
{
    this->size = size;
    data = (char *)malloc(size);
    std::copy(bytes, bytes + size, data);
}

ByteData::ByteData(const ByteData & other)
    : ByteData(other.data, other.size)
{
}

ByteData::ByteData()
{
    data = nullptr;
    size = 0;
}

ByteData::~ByteData()
{
    if (data)
        free(data);
}

const char *ByteData::GetBytes() const
{
    return data;
}

char ByteData::GetByte(size_t it) const
{
    return data[it];
}

size_t ByteData::GetSize() const
{
    return size;
}

std::string ByteData::ToString() const
{
    return std::string(GetBytes(), GetSize());
}

ByteData & ByteData::operator=(ByteData && other)
{
    this->data = other.data;
    this->size = other.size;
    other.data = nullptr;
    other.size = 0;
    return *this;
}

ByteData & ByteData::operator=(const ByteData & other)
{
    size = other.size;
    data = (char *)malloc(other.size);
    std::copy(other.data, other.data + size, data);
    return *this;
}

ByteData & ByteData::operator+=(const ByteData & other)
{
    data = (char *)realloc(data, size + other.size);
    std::copy(other.data, other.data + other.size, data + size);
    size += other.size;
    return *this;
}

const ByteData ByteData::operator+(const ByteData & other) const
{
    return ByteData(*this) += other;
}
