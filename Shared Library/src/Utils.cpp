#include "Utils.h"
#include <stdio.h>
#include <iostream>


int CreateConsole() {
    bool bResult;
    bResult = AllocConsole();
    if (!bResult)
        return -1;
    FILE *file;
    freopen_s(&file, "CONIN$", "r", stdin);
    freopen_s(&file, "CONOUT$", "w", stdout);
    freopen_s(&file, "CONOUT$", "w", stderr);
    std::ios::sync_with_stdio(true);

    return 0;
}

int PrettyPrintf(WORD wAttributes, const char *const format, ...) {
    HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_SCREEN_BUFFER_INFO consoleScreenBufferInfo;
    int iResult;

    GetConsoleScreenBufferInfo(hStdOut, &consoleScreenBufferInfo);
    SetConsoleTextAttribute(hStdOut, wAttributes);

    va_list args;
    va_start(args, format);
    iResult = vprintf(format, args);
    va_end(args);

    SetConsoleTextAttribute(hStdOut, consoleScreenBufferInfo.wAttributes);

    return iResult;
}