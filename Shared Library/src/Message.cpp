#include "Message.h"
#include <exception>
#include <limits.h>
#include <locale>
#include <algorithm>

const std::map<Message::MessageType, std::string> Message::MessageTypeNames = {
    { MessageType::Hello, "HELLO" },
    { MessageType::Message, "MESSAGE" },
    { MessageType::Bye, "BYE" },
    { MessageType::Login, "LOGIN" },
    { MessageType::Register, "REGISTER" }
};
const std::map<std::string, Message::MessageType> Message::MessageTypesByName = {
    { "HELLO", MessageType::Hello },
    { "MESSAGE", MessageType::Message },
    { "BYE", MessageType::Bye },
    { "LOGIN", MessageType::Login },
    { "REGISTER", MessageType::Register }
};

const std::string Message::MetaCharsAllowed = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_";

Message::Message(Message::MessageType type, const ByteData & data)
    : Type(type), Headers()
{
    this->SetData(Data);
}

Message::~Message()
{
}

ByteData Message::Serialize() const
{
    ByteData message;
    message += ByteData(Message::MessageTypeNames.at(Type).data());
    message += ByteData("\r\n");
    for (auto it = Headers.cbegin(); it != Headers.cend(); it++) {
        message += ByteData(it->first.data());
        message += ByteData(": ");
        message += ByteData(it->second.data());
        message += ByteData("\r\n");
    }
    message += ByteData("\r\n");
    message += Data;
    return message;
}

Message::MessageType Message::GetType() const
{
    return Type;
}

void Message::SetData(const ByteData & data)
{
    this->SetHeader("DataLength", (int64_t)data.GetSize());
    Data = data;
}

const ByteData & Message::GetData() const
{
    return Data;
}

void Message::SetMessageType(Message::MessageType type)
{
    Type = type;
}


std::string Message::HeaderValue(const std::string & header) const
{
    return Headers.at(header);
}

bool Message::HasHeader(const std::string & header) const 
{
    return Headers.find(header) != Headers.end();
}

bool Message::HeaderStringValue(const std::string & header, std::string & value) const
{
    if (!HasHeader(header))
        return false;
    value = HeaderValue(header);
    return true;
}

bool Message::HeaderInt64Value(const std::string & header, int64_t & value) const
{
    if (!HasHeader(header))
        return false;
    try {
        std::string stringValue = HeaderValue(header);
        std::string::size_type sz = 0;
        value = std::stoll(stringValue, &sz, 10);
        if (sz != stringValue.size())
            return false;
    }
    catch (std::invalid_argument &) {
        return false;
    }
    catch (std::out_of_range &) {
        return false;
    }
    return true;
}

bool Message::HeaderIntValue(const std::string & header, int & value) const
{
    int64_t value64;
    if (!HeaderInt64Value(header, value64))
        return false;
    if (value64 < INT_MIN || value64 > INT_MAX)
        return false;
    value = (int)value64;
    return true;
}

bool Message::HeaderBoolValue(const std::string & header, bool & value) const 
{
    std::string stringValue = HeaderValue(header);
    std::transform(stringValue.begin(), stringValue.end(), stringValue.begin(), ::tolower);
    if (stringValue == "true") {
        value = true;
        return true;
    }
    else if (stringValue == "false") {
        value = false;
        return true;
    }
    return false;
}

void Message::SetHeader(const std::string & header, const std::string & value)
{
    Headers[header] = value;
}

void Message::SetHeader(const std::string & header, const char * value)
{
    SetHeader(header, std::string(value));
}


void Message::SetHeader(const std::string & header, int64_t value)
{
    Headers[header] = std::to_string(value);
}

void Message::SetHeader(const std::string & header, int value)
{
    Headers[header] = std::to_string(value);
}

void Message::SetHeader(const std::string & header, bool value)
{
    Headers[header] = value ? "true" : "false";
}

void Message::RemoveHeader(const std::string & header)
{
    Headers.erase(header);
}
