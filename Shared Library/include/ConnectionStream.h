#pragma once
#include <stddef.h>
#include <WinSock2.h>
#include <stdint.h>
#include "ByteData.h"
#include "Connection.h"

class ConnectionStream
{
public:
    friend class Connection;

    ~ConnectionStream();
    int Send(const ByteData & data);
    int Receive(size_t size, ByteData & data);

    Connection * GetConnection() const;
private:
    Connection *connection;

    ConnectionStream(Connection * connection);
};

