#pragma once
#include "ByteData.h"
#include <string>
#include <map>
#include <stdint.h>

class Message
{
public:
    friend class MessageStream;
    enum class MessageType {
        Invalid,
        Hello,
        Message,
        Bye,
        Login,
        Register
    };

    static const std::map<MessageType, std::string> MessageTypeNames;
    static const std::map<std::string, MessageType> MessageTypesByName;
    static const std::string MetaCharsAllowed;
    static const size_t MaxMessageSize = 65535;

    Message(MessageType type = MessageType::Invalid, const ByteData & data = ByteData(nullptr, 0));
    ~Message();

    ByteData Serialize() const;

    void SetMessageType(MessageType type);
    MessageType GetType() const;

    void SetData(const ByteData & data);
    const ByteData & GetData() const;

    bool HeaderStringValue(const std::string & header, std::string & value) const;
    bool HeaderInt64Value(const std::string & header, int64_t & value) const;
    bool HeaderIntValue(const std::string & header, int & value) const;
    bool HeaderBoolValue(const std::string & header, bool & value) const;

    bool HasHeader(const std::string & header) const;

    void SetHeader(const std::string & header, const std::string & value);
    void SetHeader(const std::string & header, const char * value);
    void SetHeader(const std::string & header, int64_t value);
    void SetHeader(const std::string & header, int value);
    void SetHeader(const std::string & header, bool value);

    void RemoveHeader(const std::string & header);
private:
    MessageType Type;
    std::map<std::string, std::string> Headers;
    ByteData Data;
    
    std::string HeaderValue(const std::string & header) const;
};

