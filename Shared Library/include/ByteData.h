#pragma once
#include <memory>
#include <vector>
#include <string>
#include <stdint.h>

class ByteData
{
public:
    ByteData(const std::string & str);
    ByteData(const std::vector<char> & bytes);
    ByteData(const char * const bytes, size_t size);
    ByteData(const ByteData & other);
    ByteData();
    ~ByteData();

    const char * GetBytes() const;
    char GetByte(size_t it) const;
    size_t GetSize() const;
    std::string ToString() const;

    ByteData & operator=(ByteData && other);
    ByteData & operator=(const ByteData & other);

    ByteData & operator+=(const ByteData & other);
    const ByteData operator+(const ByteData & other) const;
private:
    char *data;
    size_t size;
};

