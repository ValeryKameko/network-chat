#pragma once
#include "Message.h"
#include "ConnectionStream.h"
#include <stdint.h>

class MessageStream
{

public:
    MessageStream(const std::shared_ptr<ConnectionStream> & stream);
    ~MessageStream();

    void Send(const Message & message);
    std::shared_ptr<Message> Receive();

private:
    std::shared_ptr<ConnectionStream> stream;

    static char GetByte(size_t & it, const ByteData & data);
    static bool GetSpecificByte(size_t & it, const ByteData & data, char c);
    static bool GetSpecificBytes(size_t & it, const ByteData & data, const char * s, size_t count);
    static bool GetSpecificBytes(size_t & it, const ByteData & data, const char * s);
    static bool GetByteOneOf(size_t & it, const ByteData & data, const std::string & allowed, char & readByte);
    static bool GetByteNotOneOf(size_t & it, const ByteData & data, const std::string & allowed, char & readByte);

    static bool CheckHeadersEnd(const ByteData & data);
    static bool ParseHeaderEntry(const ByteData & data, std::string & header, std::string & value);
    static bool ParseMessageName(const ByteData & data, std::string & messageName);

    bool ReceiveHeaderEntry(ByteData & data);
};

