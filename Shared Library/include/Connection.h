#pragma once
#include <memory>
#include <functional>
#include "ConnectionStream.h"

class Connection
{
public:
    friend class ConnectionStream;

    typedef std::function<void()> CloseConnectionCallback;

    Connection(SOCKET socket);
    ~Connection();
    
    std::shared_ptr<ConnectionStream> GetStream();

    int GetHostPort();
    IN_ADDR GetHostIP();
    std::string GetHostIPString();

    int GetPeerPort();
    IN_ADDR GetPeerIP();
    std::string GetPeerIPString();

    void AddCloseConnectionCallback(const CloseConnectionCallback &callback);
    void Close();

private:
    SOCKET socket;
    std::shared_ptr<ConnectionStream> connectionStream;

    SOCKADDR_IN GetHostAddress(SOCKET socket);
    SOCKADDR_IN GetPeerAddress(SOCKET socket);
    std::vector<CloseConnectionCallback> onCloseConnection;
    bool IsEstablished;
};

