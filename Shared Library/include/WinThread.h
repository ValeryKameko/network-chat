#pragma once
#include <windows.h>

class WinThread
{
public:
    WinThread();
    ~WinThread();

    virtual void Run() = 0;

    DWORD GetId();
    void Suspend();
    void Start();
    void Resume();
    void Terminate(DWORD dwCode);
    void Wait();

protected:
    void Exit(DWORD dwCode);
    virtual void OnExit();

private:
    DWORD ThreadId;
    HANDLE hThread;

    static DWORD WINAPI ThreadProc(LPVOID lpParameter);
    void OnInternalExit(DWORD dwCode);
};

