#include "ChatServerApplication.h"
#include "Utils.h"


ChatServerApplication::ChatServerApplication(HINSTANCE hInstance, PWSTR pCmdLine)
    : hInstance(hInstance), pCmdLine(pCmdLine)
{
    CreateConsole();
    InitializeWinSocket(WSAVersion);
    this->Chat = std::make_shared<::Chat>();
    server = std::make_unique<ChatServer>(this->Chat, DefaultPort);
}


ChatServerApplication::~ChatServerApplication()
{
    WSACleanup();
}

int ChatServerApplication::Run()
{
    server->Start();
    server->Wait();
    return 0;
}

void ChatServerApplication::InitializeWinSocket(WORD wVersion)
{
    WSADATA wsaData;
    WSAStartup(wVersion, &wsaData);
}