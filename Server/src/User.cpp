#include "User.h"



User::User(const std::string & name, const std::string & password)
    : Name(name), Password(password)
{
    ChangeStatus(Status::Online);
    connection.reset();
}

User::~User()
{
}

User::LoginResult User::Login(const std::shared_ptr<Connection> & connection, const std::string & password)
{
    if (CurrentStatus == User::Status::Online)
        return User::LoginResult::AlreadyLogon;
    if (password == Password) {
        this->connection = connection;
        ChangeStatus(Status::Online);
        return User::LoginResult::Ok;
    }
    return User::LoginResult::PasswordMistmach;
}

void User::Logout()
{
    if (CurrentStatus == Status::Online) {
        connection.reset();
        ChangeStatus(Status::Offline);
    }
}

void User::AddChangeStatusCallback(const ChangeStatusCallback & callback)
{
    OnChangeStatus.push_back(callback);
}

const std::string & User::GetName() const
{
    return Name;
}

void User::SetConnection(const std::shared_ptr<Connection>& connection)
{
    this->connection = connection;
}

const std::shared_ptr<Connection> User::GetConnection() const
{
    return connection;
}

void User::ChangeStatus(Status newStatus)
{
    CurrentStatus = newStatus;
    for (auto & callback : OnChangeStatus) {
        callback(newStatus);
    }
}