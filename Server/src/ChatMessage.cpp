#include "ChatMessage.h"



ChatMessage::ChatMessage(const std::shared_ptr<User> & author, const std::string & data)
    : Author(author), Data(data)
{
}

ChatMessage::~ChatMessage()
{
}

const std::shared_ptr<User> & ChatMessage::GetAuthor() const
{
    return Author;
}

const std::string & ChatMessage::GetData() const
{
    return Data;
}
