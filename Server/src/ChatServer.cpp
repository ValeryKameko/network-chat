#include "ChatServer.h"
#include <functional>
#include "ConnectionStream.h"
#include "ClientHandler.h"

ChatServer::ChatServer(const std::shared_ptr<::Chat> & chat, int defaultPort)
    : WinThread()
{
    this->Chat = chat;
    SOCKET listenSocket = CreateListenSocket(defaultPort);
    if (listenSocket == INVALID_SOCKET) {
        listenSocket = CreateFreeListenSocket();
    }
    acceptor = std::make_unique<ConnectionAcceptor>(listenSocket);
    printf("Server started at port %d\n", GetHostPort(listenSocket));
}

ChatServer::~ChatServer() 
{

}

void ChatServer::Run()
{
    acceptor->SetAcceptConnectionCallback(
        std::bind(&ChatServer::HandleAcceptingConnection, this, std::placeholders::_1));

    acceptor->Run();
}

SOCKET ChatServer::CreateFreeListenSocket()
{
    return CreateListenSocket(0);
}

SOCKET ChatServer::CreateListenSocket(int port)
{
    int iResult;
    SOCKADDR_IN address;
    address.sin_family = AF_INET;
    address.sin_port = htons(port);
    address.sin_addr.s_addr = INADDR_ANY;

    SOCKET socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (socket == INVALID_SOCKET)
        return INVALID_SOCKET;

    iResult = ::bind(socket, reinterpret_cast<SOCKADDR *>(&address), sizeof(address));
    if (iResult == SOCKET_ERROR) {
        closesocket(socket);
        return INVALID_SOCKET;
    }
    return socket;
}

SOCKADDR_IN ChatServer::GetHostAddress(SOCKET socket)
{
    SOCKADDR_IN address;
    int size = sizeof(address);
    getsockname(socket, (SOCKADDR *)&address, &size);
    return address;
}

int ChatServer::GetHostPort(SOCKET socket)
{
    return ntohs(GetHostAddress(socket).sin_port);
}

IN_ADDR ChatServer::GetHostIP(SOCKET socket)
{
    return GetHostAddress(socket).sin_addr;
}

void ChatServer::HandleAcceptingConnection(const std::shared_ptr<Connection> &connection)
{
    ClientHandler *handler = new ClientHandler(connection, this->Chat);
    handler->Start();
}
