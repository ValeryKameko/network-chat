#include "ConnectionAcceptor.h"

ConnectionAcceptor::ConnectionAcceptor(SOCKET socket)
    : WinThread(), socket(socket)
{
    int iResult;
    iResult = ::listen(socket, ConnectionQueueSize);
}

ConnectionAcceptor::~ConnectionAcceptor()
{
    closesocket(socket);
}

void ConnectionAcceptor::SetAcceptConnectionCallback(const AcceptConnectionCallback &callback)
{
    onAcceptConnection = callback;
}

void ConnectionAcceptor::Run()
{
    SOCKADDR_IN socketAddress;
    int socketAddressSize = sizeof(socketAddress);
    SOCKET acceptedSocket;
    while (true) {
        acceptedSocket = ::accept(socket, (SOCKADDR *)&socketAddress, &socketAddressSize);
        if (acceptedSocket != INVALID_SOCKET) {
            std::shared_ptr<Connection> connection = nullptr;
            connection = std::make_shared<Connection>(acceptedSocket);
            onAcceptConnection(connection);
        }
    }
}
