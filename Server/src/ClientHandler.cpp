#include "ClientHandler.h"
#include "MessageStream.h"
#include <functional>
#include <WinSock2.h>
#include <algorithm>
#include <cctype>

void ClientHandler::HandleHelloMessage(const Message & message)
{
    if (IsConnectionEstablished)
        Reject();
    Message sendMessage(Message::MessageType::Hello);
    this->MessageStream->Send(sendMessage);
    IsConnectionEstablished = true;
}

void ClientHandler::HandleByeMessage(const Message & message)
{
    if (this->User)
        this->User->Logout();

    Message sendMessage(Message::MessageType::Bye);
    this->MessageStream->Send(sendMessage);

    Reject();
}

void ClientHandler::HandleMessageMessage(const Message & message)
{
    if (!this->User)
        Reject();
    
    this->Chat->HandleMessage(ChatMessage(this->User, message.GetData().ToString()));
}

void ClientHandler::HandleLoginMessage(const Message & message)
{
    Message sendMessage;
    if (!IsConnectionEstablished)
        Reject();

    std::string username;
    std::string password;
    if (!message.HeaderStringValue("Username", username))
        Reject();
    if (!message.HeaderStringValue("Password", password))
        Reject();

    std::shared_ptr<::User> user = Chat->GetUser(username);
    if (!user) {
        sendMessage.SetMessageType(Message::MessageType::Login);
        sendMessage.SetHeader("Status", false);
        sendMessage.SetHeader("Reason", "No such user");
        this->MessageStream->Send(sendMessage);
        return;
    }

    User::LoginResult result = user->Login(Connection, password);
    switch (result) {
        case User::LoginResult::AlreadyLogon:
            sendMessage.SetMessageType(Message::MessageType::Login);
            sendMessage.SetHeader("Status", false);
            sendMessage.SetHeader("Reason", "Already logon");
            this->MessageStream->Send(sendMessage);
            return;
        case User::LoginResult::PasswordMistmach:
            sendMessage.SetMessageType(Message::MessageType::Login);
            sendMessage.SetHeader("Status", false);
            sendMessage.SetHeader("Reason", "Password mistmatch");
            this->MessageStream->Send(sendMessage);
            return;
        case User::LoginResult::Ok:
            sendMessage.SetMessageType(Message::MessageType::Login);
            sendMessage.SetHeader("Status", true);
            this->MessageStream->Send(sendMessage);
            break;
    }

    this->User = user;
}

void ClientHandler::HandleRegisterMessage(const Message & message)
{
    Message sendMessage;
    if (!IsConnectionEstablished)
        Reject();
    std::string username;
    std::string password;
    if (!message.HeaderStringValue("Username", username))
        Reject();
    if (!message.HeaderStringValue("Password", password))
        Reject();
       
    bool isUsernameValid = 
        username.size() > 0 &&
        std::all_of(username.begin(), username.end(), [](char c) {
            return std::isprint(c);
        });
    if (!isUsernameValid) {
        sendMessage.SetMessageType(Message::MessageType::Register);
        sendMessage.SetHeader("Status", false);
        sendMessage.SetHeader("Reason", std::string("Username invalid"));
        this->MessageStream->Send(sendMessage);
        return;
    }

    bool isPasswordValid = password.size() > 0;
    if (!isPasswordValid) {
        sendMessage.SetMessageType(Message::MessageType::Register);
        sendMessage.SetHeader("Status", false);
        sendMessage.SetHeader("Reason", std::string("Password invalid"));
        this->MessageStream->Send(sendMessage);
        return;
    }

    this->User = std::make_shared<::User>(username, password);
    this->User->SetConnection(this->Connection);
    if (!this->Chat->AddUser(this->User)) {
        this->User = nullptr;
        sendMessage.SetMessageType(Message::MessageType::Register);
        sendMessage.SetHeader("Status", false);
        sendMessage.SetHeader("Reason", std::string("User already exists"));
        this->MessageStream->Send(sendMessage);
        return;
    }

    sendMessage.SetMessageType(Message::MessageType::Register);
    sendMessage.SetHeader("Status", true);
    this->MessageStream->Send(sendMessage);
}

ClientHandler::ClientHandler(
    const std::shared_ptr<::Connection> & connection,
    const std::shared_ptr<::Chat> & chat)
{
    this->Connection = connection;
    this->Chat = chat;
    this->Connection->AddCloseConnectionCallback(
        std::bind(&ClientHandler::Reject, this));

    this->User = nullptr;
    IsConnectionEstablished = false;

    auto stream = this->Connection->GetStream();
    this->MessageStream = std::make_shared<::MessageStream>(stream);
}


ClientHandler::~ClientHandler()
{
    MessageStream.reset();
    User.reset();
    Chat.reset();
    Connection.reset();
    this->Exit(2);
}

void ClientHandler::Run()
{
    printf("New Connection from %15s : %5d\n",
        this->Connection->GetPeerIPString().c_str(),
        this->Connection->GetPeerPort());

    while (true) {        
        std::shared_ptr<::Message> message = this->MessageStream->Receive();
        if (!message)
            break;
        ByteData serializedMessage = message->Serialize();
        printf("[%15s : %5d]: {START MESSAGE}\n",
            this->Connection->GetPeerIPString().c_str(),
            this->Connection->GetPeerPort());
        printf("%.*s\n",
            serializedMessage.GetSize(),
            serializedMessage.GetBytes());
        printf("{END MESSAGE}\n");

        switch (message->GetType()) {
        case Message::MessageType::Invalid:
            Reject();
            break;
        case Message::MessageType::Hello:
            HandleHelloMessage(*message);
            break;
        case Message::MessageType::Bye:
            HandleHelloMessage(*message);
            break;
        case Message::MessageType::Message:
            HandleMessageMessage(*message);
            break;
        case Message::MessageType::Login:
            HandleLoginMessage(*message);
            break;
        case Message::MessageType::Register:
            HandleRegisterMessage(*message);
            break;
        default:
            Reject();
        }
    }
    this->Reject();
}

void ClientHandler::Reject()
{
    if (User)
        User->Logout();

    printf("Close Connection from %15s : %5d\n",
        this->Connection->GetPeerIPString().c_str(),
        this->Connection->GetPeerPort());

    delete this;
}