#include "Chat.h"
#include "MessageStream.h"
#include "Message.h"

Chat::Chat()
{
}


Chat::~Chat()
{
}

bool Chat::AddUser(const std::shared_ptr<User>& user)
{
    if (Users.find(user->GetName()) == Users.end())
        Users[user->GetName()] = user;
    else
        return false;

    this->OnlineUsers.insert(user);
    user->AddChangeStatusCallback([this, user](User::Status status) {
        switch (status) {
        case User::Status::Online:
            this->OnlineUsers.insert(user);
            break;
        case User::Status::Offline:
            this->OnlineUsers.erase(user);
            break;
        }
    });

    return true;
}

void Chat::HandleMessage(const ChatMessage & message)
{
    Message sendMessage(Message::MessageType::Message);
    sendMessage.SetHeader("Author", message.GetAuthor()->GetName());
    sendMessage.SetData(ByteData(message.GetData()));

    for (auto & user : OnlineUsers) {
        MessageStream stream(user->GetConnection()->GetStream());
        sendMessage.SetHeader("Owner", user->GetName() == message.GetAuthor()->GetName());
        stream.Send(sendMessage);
    }
}

std::shared_ptr<User> Chat::GetUser(const std::string username) const
{
    auto it = Users.find(username);
    return it == Users.end() ? nullptr : it->second;
}
