#include <windows.h>
#include "Utils.h"
#include "ChatServerApplication.h"

int WINAPI wWinMain(
    HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    PWSTR pCmdLine,
    int nCmdShow)
{
    ChatServerApplication application(hInstance, pCmdLine);
    return application.Run();
}