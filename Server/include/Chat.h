#pragma once
#include "User.h"
#include "ChatMessage.h"
#include <memory>
#include <map>
#include <set>
#include <string>

class Chat
{
public:
    Chat();
    ~Chat();

    bool AddUser(const std::shared_ptr<User> & user);
    void HandleMessage(const ChatMessage & message);

    std::shared_ptr<User> GetUser(const std::string username) const;
private:
    std::map<std::string, std::shared_ptr<User> > Users;
    std::set<std::shared_ptr<User> > OnlineUsers;
};

