#pragma once
#include <vector>
#include <functional>
#include <windows.h>
#include "MessageStream.h"

class User
{
public:
    enum class Status {
        Online,
        Offline
    };
    enum class LoginResult {
        Ok,
        AlreadyLogon,
        PasswordMistmach
    };
    typedef std::function<void(Status)> ChangeStatusCallback;

    User(const std::string & name, const std::string & password);
    ~User();

    LoginResult Login(const std::shared_ptr<Connection> & connection, const std::string & password);
    void Logout();

    void AddChangeStatusCallback(const ChangeStatusCallback & callback);

    const std::string & GetName() const;
    void SetConnection(const std::shared_ptr<Connection> & connection);
    const std::shared_ptr<Connection> GetConnection() const;
private:
    std::vector<ChangeStatusCallback> OnChangeStatus;

    Status CurrentStatus;

    std::shared_ptr<Connection> connection;

    std::string Name;
    std::string Password;

    void ChangeStatus(Status newStatus);
};

