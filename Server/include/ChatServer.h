#pragma once
#include <memory>
#include "ConnectionAcceptor.h"
#include "WinThread.h"
#include "Chat.h"

class ChatServer : public WinThread
{
private:
    std::shared_ptr<::Chat> Chat;
    std::unique_ptr<ConnectionAcceptor> acceptor;

    SOCKET CreateListenSocket(int port);
    SOCKET CreateFreeListenSocket();

    SOCKADDR_IN GetHostAddress(SOCKET socket);
    int GetHostPort(SOCKET socket);
    IN_ADDR GetHostIP(SOCKET socket);

    void HandleAcceptingConnection(const std::shared_ptr<Connection> &connection);
public:
    ChatServer(const std::shared_ptr<::Chat> & chat, int defaultPort);
    ~ChatServer();
    void Run();
};
