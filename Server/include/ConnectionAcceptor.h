#pragma once
#include <memory>
#include <WinSock2.h>
#include <functional>
#include "Connection.h"
#include "WinThread.h"

class ConnectionAcceptor : public WinThread
{
private:
    typedef std::function<void(const std::shared_ptr<Connection> &)> AcceptConnectionCallback;

    static const int ConnectionQueueSize = 5;
    SOCKET socket;

    AcceptConnectionCallback onAcceptConnection;
protected:

public:
    ConnectionAcceptor(SOCKET socket);
    ~ConnectionAcceptor();
    void SetAcceptConnectionCallback(const AcceptConnectionCallback &callback);
    void Run() override;
};