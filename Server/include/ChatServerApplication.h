#pragma once
#include <windows.h>
#include "ChatServer.h" 

class ChatServerApplication
{
private:
    static const int DefaultPort = 12345;
    static const WORD WSAVersion = MAKEWORD(2, 2);

    HINSTANCE hInstance;
    PWSTR pCmdLine;

    std::unique_ptr<ChatServer> server;
    std::shared_ptr<::Chat>  Chat;

    void InitializeWinSocket(WORD wVersion);
public:
    ChatServerApplication(HINSTANCE hInstance, PWSTR pCmdLine);
    ~ChatServerApplication();
    int Run();
};
