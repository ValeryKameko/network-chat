#pragma once
#include "User.h"

class ChatMessage
{
public:
    ChatMessage(const std::shared_ptr<User> & author, const std::string & data);
    ~ChatMessage();

    const std::shared_ptr<User> & GetAuthor() const;
    const std::string & GetData() const;

private:
    std::shared_ptr<User> Author;
    std::string Data;
};

