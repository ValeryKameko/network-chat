#pragma once
#include <memory>
#include "Connection.h"
#include "WinThread.h"
#include "User.h"
#include "Chat.h"

class ClientHandler : public WinThread
{
private:
    bool IsConnectionEstablished;
    std::shared_ptr<::Connection> Connection;
    std::shared_ptr<::MessageStream> MessageStream;
    std::shared_ptr<::User> User;
    std::shared_ptr<::Chat> Chat;

    void HandleHelloMessage(const Message & message);
    void HandleByeMessage(const Message & message);
    void HandleMessageMessage(const Message & message);
    void HandleLoginMessage(const Message & message);
    void HandleRegisterMessage(const Message & message);

public:
    ClientHandler(
        const std::shared_ptr<::Connection> & connection, 
        const std::shared_ptr<::Chat> & chat);
    ~ClientHandler();

    void Run() override;
    void Reject();
};

