#pragma once

#include "LoginForm.h"
#include "RegisterForm.h"
#include "RegisterForm.h"
#include "ChatForm.h"

namespace Client {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� MainForm
	/// </summary>
	public ref class MainForm : public System::Windows::Forms::Form
	{
	public:
		MainForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MainForm()
		{
			if (components)
			{
				delete components;
			}
		}
    private: System::Windows::Forms::Button^  buttonLogin;
    protected:

    private: System::Windows::Forms::Button^  buttonRegister;
    private: System::Windows::Forms::Button^  buttonExit;
    protected:


    private: System::Windows::Forms::TableLayoutPanel^  MenuLayout;

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
            this->buttonLogin = (gcnew System::Windows::Forms::Button());
            this->buttonRegister = (gcnew System::Windows::Forms::Button());
            this->buttonExit = (gcnew System::Windows::Forms::Button());
            this->MenuLayout = (gcnew System::Windows::Forms::TableLayoutPanel());
            this->MenuLayout->SuspendLayout();
            this->SuspendLayout();
            // 
            // buttonLogin
            // 
            this->buttonLogin->Anchor = System::Windows::Forms::AnchorStyles::None;
            this->buttonLogin->FlatStyle = System::Windows::Forms::FlatStyle::System;
            this->buttonLogin->Location = System::Drawing::Point(69, 139);
            this->buttonLogin->Name = L"buttonLogin";
            this->buttonLogin->Size = System::Drawing::Size(248, 69);
            this->buttonLogin->TabIndex = 0;
            this->buttonLogin->Text = L"Login";
            this->buttonLogin->UseVisualStyleBackColor = true;
            this->buttonLogin->Click += gcnew System::EventHandler(this, &MainForm::buttonLogin_Click);
            // 
            // buttonRegister
            // 
            this->buttonRegister->Anchor = System::Windows::Forms::AnchorStyles::None;
            this->buttonRegister->FlatStyle = System::Windows::Forms::FlatStyle::System;
            this->buttonRegister->Location = System::Drawing::Point(69, 23);
            this->buttonRegister->Name = L"buttonRegister";
            this->buttonRegister->Size = System::Drawing::Size(248, 69);
            this->buttonRegister->TabIndex = 1;
            this->buttonRegister->Text = L"Register";
            this->buttonRegister->UseVisualStyleBackColor = true;
            this->buttonRegister->Click += gcnew System::EventHandler(this, &MainForm::buttonRegister_Click);
            // 
            // buttonExit
            // 
            this->buttonExit->Anchor = System::Windows::Forms::AnchorStyles::None;
            this->buttonExit->FlatStyle = System::Windows::Forms::FlatStyle::System;
            this->buttonExit->Location = System::Drawing::Point(69, 256);
            this->buttonExit->Name = L"buttonExit";
            this->buttonExit->Size = System::Drawing::Size(248, 69);
            this->buttonExit->TabIndex = 2;
            this->buttonExit->Text = L"Exit";
            this->buttonExit->UseVisualStyleBackColor = true;
            this->buttonExit->Click += gcnew System::EventHandler(this, &MainForm::buttonExit_Click);
            // 
            // MenuLayout
            // 
            this->MenuLayout->ColumnCount = 1;
            this->MenuLayout->ColumnStyles->Add((gcnew System::Windows::Forms::ColumnStyle()));
            this->MenuLayout->Controls->Add(this->buttonRegister, 0, 0);
            this->MenuLayout->Controls->Add(this->buttonLogin, 1, 0);
            this->MenuLayout->Controls->Add(this->buttonExit, 0, 1);
            this->MenuLayout->Dock = System::Windows::Forms::DockStyle::Fill;
            this->MenuLayout->Location = System::Drawing::Point(0, 0);
            this->MenuLayout->Name = L"MenuLayout";
            this->MenuLayout->RowCount = 2;
            this->MenuLayout->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 33.33333F)));
            this->MenuLayout->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 33.33333F)));
            this->MenuLayout->RowStyles->Add((gcnew System::Windows::Forms::RowStyle(System::Windows::Forms::SizeType::Percent, 33.33333F)));
            this->MenuLayout->Size = System::Drawing::Size(387, 350);
            this->MenuLayout->TabIndex = 3;
            // 
            // MainForm
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(387, 350);
            this->Controls->Add(this->MenuLayout);
            this->Name = L"MainForm";
            this->Text = L"MainForm";
            this->MenuLayout->ResumeLayout(false);
            this->ResumeLayout(false);

        }
#pragma endregion

    private: System::Void buttonRegister_Click(System::Object^  sender, System::EventArgs^  e) {
        RegisterForm^ registerForm = gcnew RegisterForm();
        this->Hide();
        registerForm->ShowDialog();
        this->Show();
        if (registerForm->Connection) {
            ChatForm^ form = gcnew ChatForm(registerForm->Connection);
            form->ShowDialog();
        }
    }
    private: System::Void buttonExit_Click(System::Object^  sender, System::EventArgs^  e) {
        this->Close();
    }
    private: System::Void buttonLogin_Click(System::Object^  sender, System::EventArgs^  e) {
        LoginForm^ loginForm = gcnew LoginForm();
        this->Hide();
        loginForm->ShowDialog();
        this->Show();
        if (loginForm->Connection) {
            ChatForm^ form = gcnew ChatForm(loginForm->Connection);
            form->ShowDialog();
        }
    }
    };
}
