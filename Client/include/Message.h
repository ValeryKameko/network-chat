#pragma once


namespace Client {
    using namespace System;
    using namespace System::Collections::Generic;
    using namespace System::Text;

    using ByteData = array<Byte>;

    public ref class Message
    {
    public:
        enum class MessageType {
            Invalid,
            Hello,
            Message,
            Bye,
            Login,
            Register
        };

        static Dictionary<MessageType, String^>^ MessageTypeNames = MessageTypeNamesInitializer();
        static Dictionary<MessageType, String^>^ MessageTypeNamesInitializer()
        {
            Dictionary<MessageType, String^>^ dict = gcnew Dictionary<MessageType, String^>();
            dict->Add(MessageType::Hello, "HELLO");
            dict->Add(MessageType::Message, "MESSAGE");
            dict->Add(MessageType::Bye, "BYE");
            dict->Add(MessageType::Login, "LOGIN");
            dict->Add(MessageType::Register, "REGISTER");
            return dict;
        };

        static Dictionary<String^, MessageType>^ MessageTypesByName = MessageTypesByNameInitializer();
        static Dictionary<String^, MessageType>^ MessageTypesByNameInitializer()
        {
            Dictionary<String^, MessageType>^ dict = gcnew Dictionary<String^, MessageType>();
            dict->Add("HELLO", MessageType::Hello);
            dict->Add("MESSAGE", MessageType::Message);
            dict->Add("BYE", MessageType::Bye);
            dict->Add("LOGIN", MessageType::Login);
            dict->Add("REGISTER", MessageType::Register);
            return dict;
        };

        static String^ MetaCharsAllowed = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_";
        static int MaxMessageSize = 65535;

        Message(MessageType type, ByteData^ data, Dictionary<String^, String^>^ headers)
        {
            Type = type;
            Headers = gcnew Dictionary<String^, String^>();
            for each (KeyValuePair<String^, String^>^ headerEntry in headers)
                Headers->Add(gcnew String(headerEntry->Key), gcnew String(headerEntry->Value));
            SetData(data);
        }

        Message(MessageType type, ByteData^ data)
            : Message(type, data, gcnew Dictionary<String^, String^>())
        {
        }

        Message(MessageType type)
            : Message(type, gcnew ByteData(0))
        {
        }

        Message()
            : Message(MessageType::Invalid)
        {
        }

        void SetData(ByteData^ data)
        {
            SetHeader("DataLength", data->Length);
            Array::Resize(Data, data->Length);
            Array::Copy(data, Data, data->Length);
        }

        ByteData^ GetData()
        {
            return Data;
        }

        void SetType(MessageType type)
        {
            Type = type;
        }

        MessageType GetType()
        {
            return Type;
        }

        String^ HeaderValue(String^ header)
        {
            return Headers[header];
        }

        Boolean HasHeader(String^ header)
        {
            return Headers->ContainsKey(header);
        }

        String^ HeaderStringValue(String^ header)
        {
            if (!HasHeader(header))
                return nullptr;
            return HeaderValue(header);
        }

        Int64^ Message::HeaderInt64Value(String^ header)
        {
            if (!HasHeader(header))
                return nullptr;
            Int64 value;
            if (!Int64::TryParse(HeaderValue(header), value))
                return nullptr;
            return value;
        }

        int^ Message::HeaderIntValue(String^ header)
        {
            if (!HasHeader(header))
                return nullptr;
            int value;
            if (!int::TryParse(HeaderValue(header), value))
                return nullptr;
            return value;
        }

        Boolean^ Message::HeaderBoolValue(String^ header)
        {
            if (!HasHeader(header))
                return nullptr;
            String^ value = HeaderValue(header);
            if (value->ToLower() == "true")
                return true;
            else if (value->ToLower() == "false")
                return false;
            return nullptr;
        }

        Void SetHeader(String^ header, String^ value)
        {
            Headers[header] = value;
        }

        Void SetHeader(String^ header, Int64^ value)
        {
            SetHeader(header, value->ToString());
        }

        Void SetHeader(String^ header, int^ value)
        {
            SetHeader(header, value->ToString());
        }

        Void SetHeader(String^ header, Boolean^ value)
        {
            SetHeader(header, value ? "true" : "false");
        }

        ByteData^ Serialize()
        {
            List<Byte>^ serializedMessage = gcnew List<Byte>();
            serializedMessage->AddRange(Encoding::ASCII->GetBytes(MessageTypeNames[Type]));
            serializedMessage->AddRange(Encoding::ASCII->GetBytes("\r\n"));
            for each (KeyValuePair<String^, String^>^ entry in Headers) {
                serializedMessage->AddRange(Encoding::ASCII->GetBytes(entry->Key + ": " + entry->Value + "\r\n"));
            }
            serializedMessage->AddRange(Encoding::ASCII->GetBytes("\r\n"));
            serializedMessage->AddRange(Data);
            return serializedMessage->ToArray();
        }

    private:
        MessageType Type;
        Dictionary<String^, String^>^ Headers;
        ByteData^ Data;
    };

}