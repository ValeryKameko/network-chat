#pragma once

#include "Message.h"
#include "MessageStream.h"
#include "ServerConnection.h"

namespace Client {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
    using namespace System::Collections;

	/// <summary>
	/// ������ ��� RegisterForm
	/// </summary>
	public ref class RegisterForm : public System::Windows::Forms::Form
	{
	public:
		RegisterForm(void)
		{
			InitializeComponent();

            errorProviderServerIP = gcnew ErrorProvider();
            errorProviderServerIP->SetIconAlignment(this->textboxServerIP, ErrorIconAlignment::MiddleRight);
            errorProviderServerIP->SetIconPadding(this->textboxServerIP, 2);
            errorProviderServerIP->BlinkStyle = ErrorBlinkStyle::NeverBlink;

            errorProviderServerPort = gcnew ErrorProvider();
            errorProviderServerPort->SetIconAlignment(this->textboxServerPort, ErrorIconAlignment::MiddleRight);
            errorProviderServerPort->SetIconPadding(this->textboxServerPort, 2);
            errorProviderServerPort->BlinkStyle = ErrorBlinkStyle::NeverBlink;

            errorProviderUsername = gcnew ErrorProvider();
            errorProviderUsername->SetIconAlignment(this->textboxUsername, ErrorIconAlignment::MiddleRight);
            errorProviderUsername->SetIconPadding(this->textboxUsername, 2);
            errorProviderUsername->BlinkStyle = ErrorBlinkStyle::NeverBlink;

            errorProviderPassword = gcnew ErrorProvider();
            errorProviderPassword->SetIconAlignment(this->textboxPassword, ErrorIconAlignment::MiddleRight);
            errorProviderPassword->SetIconPadding(this->textboxPassword, 2);
            errorProviderPassword->BlinkStyle = ErrorBlinkStyle::NeverBlink;

            textboxUsername->Focus();
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~RegisterForm()
		{
			if (components)
			{
				delete components;
			}
		}
    private: System::Windows::Forms::Button^  buttonBack;
    private: System::Windows::Forms::Button^  buttonRegister;

    private: System::Windows::Forms::TextBox^  textboxPassword;
    private: System::Windows::Forms::TextBox^  textboxUsername;
    private: System::Windows::Forms::TextBox^  textboxServerPort;
    private: System::Windows::Forms::Label^  labelPassword;
    private: System::Windows::Forms::Label^  labelUsername;
    private: System::Windows::Forms::Label^  labelServerPort;
    private: System::Windows::Forms::Label^  labelServerIP;
    private: System::Windows::Forms::TextBox^  textboxServerIP;

    private:
        System::Windows::Forms::ErrorProvider^ errorProviderServerIP;
        System::Windows::Forms::ErrorProvider^ errorProviderServerPort;
        System::Windows::Forms::ErrorProvider^ errorProviderUsername;
        System::Windows::Forms::ErrorProvider^ errorProviderPassword;


	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
            this->buttonBack = (gcnew System::Windows::Forms::Button());
            this->buttonRegister = (gcnew System::Windows::Forms::Button());
            this->textboxPassword = (gcnew System::Windows::Forms::TextBox());
            this->textboxUsername = (gcnew System::Windows::Forms::TextBox());
            this->textboxServerPort = (gcnew System::Windows::Forms::TextBox());
            this->labelPassword = (gcnew System::Windows::Forms::Label());
            this->labelUsername = (gcnew System::Windows::Forms::Label());
            this->labelServerPort = (gcnew System::Windows::Forms::Label());
            this->labelServerIP = (gcnew System::Windows::Forms::Label());
            this->textboxServerIP = (gcnew System::Windows::Forms::TextBox());
            this->SuspendLayout();
            // 
            // buttonBack
            // 
            this->buttonBack->Location = System::Drawing::Point(227, 204);
            this->buttonBack->Name = L"buttonBack";
            this->buttonBack->Size = System::Drawing::Size(107, 38);
            this->buttonBack->TabIndex = 19;
            this->buttonBack->Text = L"Back";
            this->buttonBack->UseVisualStyleBackColor = true;
            this->buttonBack->Click += gcnew System::EventHandler(this, &RegisterForm::buttonBack_Click);
            // 
            // buttonRegister
            // 
            this->buttonRegister->Location = System::Drawing::Point(32, 204);
            this->buttonRegister->Name = L"buttonRegister";
            this->buttonRegister->Size = System::Drawing::Size(107, 38);
            this->buttonRegister->TabIndex = 18;
            this->buttonRegister->Text = L"Register";
            this->buttonRegister->UseVisualStyleBackColor = true;
            this->buttonRegister->Click += gcnew System::EventHandler(this, &RegisterForm::buttonRegister_Click);
            // 
            // textboxPassword
            // 
            this->textboxPassword->Location = System::Drawing::Point(153, 164);
            this->textboxPassword->MaxLength = 20;
            this->textboxPassword->Name = L"textboxPassword";
            this->textboxPassword->Size = System::Drawing::Size(140, 20);
            this->textboxPassword->TabIndex = 17;
            this->textboxPassword->UseSystemPasswordChar = true;
            this->textboxPassword->Validating += gcnew System::ComponentModel::CancelEventHandler(this, &RegisterForm::textboxPassword_Validating);
            this->textboxPassword->Validated += gcnew System::EventHandler(this, &RegisterForm::textboxPassword_Validated);
            // 
            // textboxUsername
            // 
            this->textboxUsername->Location = System::Drawing::Point(153, 123);
            this->textboxUsername->MaxLength = 30;
            this->textboxUsername->Name = L"textboxUsername";
            this->textboxUsername->Size = System::Drawing::Size(140, 20);
            this->textboxUsername->TabIndex = 16;
            this->textboxUsername->Validating += gcnew System::ComponentModel::CancelEventHandler(this, &RegisterForm::textboxUsername_Validating);
            this->textboxUsername->Validated += gcnew System::EventHandler(this, &RegisterForm::textboxUsername_Validated);
            // 
            // textboxServerPort
            // 
            this->textboxServerPort->Location = System::Drawing::Point(153, 82);
            this->textboxServerPort->MaxLength = 5;
            this->textboxServerPort->Name = L"textboxServerPort";
            this->textboxServerPort->Size = System::Drawing::Size(51, 20);
            this->textboxServerPort->TabIndex = 15;
            this->textboxServerPort->Text = L"12345";
            this->textboxServerPort->Validating += gcnew System::ComponentModel::CancelEventHandler(this, &RegisterForm::textboxServerPort_Validating);
            this->textboxServerPort->Validated += gcnew System::EventHandler(this, &RegisterForm::textboxServerPort_Validated);
            // 
            // labelPassword
            // 
            this->labelPassword->AutoSize = true;
            this->labelPassword->Location = System::Drawing::Point(50, 167);
            this->labelPassword->Name = L"labelPassword";
            this->labelPassword->Size = System::Drawing::Size(53, 13);
            this->labelPassword->TabIndex = 14;
            this->labelPassword->Text = L"Password";
            // 
            // labelUsername
            // 
            this->labelUsername->AutoSize = true;
            this->labelUsername->Location = System::Drawing::Point(50, 126);
            this->labelUsername->Name = L"labelUsername";
            this->labelUsername->Size = System::Drawing::Size(55, 13);
            this->labelUsername->TabIndex = 13;
            this->labelUsername->Text = L"Username";
            // 
            // labelServerPort
            // 
            this->labelServerPort->AutoSize = true;
            this->labelServerPort->Location = System::Drawing::Point(50, 85);
            this->labelServerPort->Name = L"labelServerPort";
            this->labelServerPort->Size = System::Drawing::Size(60, 13);
            this->labelServerPort->TabIndex = 12;
            this->labelServerPort->Text = L"Server Port";
            // 
            // labelServerIP
            // 
            this->labelServerIP->AutoSize = true;
            this->labelServerIP->Location = System::Drawing::Point(50, 44);
            this->labelServerIP->Name = L"labelServerIP";
            this->labelServerIP->Size = System::Drawing::Size(51, 13);
            this->labelServerIP->TabIndex = 11;
            this->labelServerIP->Text = L"Server IP";
            // 
            // textboxServerIP
            // 
            this->textboxServerIP->Location = System::Drawing::Point(153, 41);
            this->textboxServerIP->MaxLength = 15;
            this->textboxServerIP->Name = L"textboxServerIP";
            this->textboxServerIP->Size = System::Drawing::Size(122, 20);
            this->textboxServerIP->TabIndex = 10;
            this->textboxServerIP->Text = L"127.0.0.1";
            this->textboxServerIP->Validating += gcnew System::ComponentModel::CancelEventHandler(this, &RegisterForm::textboxServerIP_Validating);
            this->textboxServerIP->Validated += gcnew System::EventHandler(this, &RegisterForm::textboxServerIP_Validated);
            // 
            // RegisterForm
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(358, 261);
            this->Controls->Add(this->buttonBack);
            this->Controls->Add(this->buttonRegister);
            this->Controls->Add(this->textboxPassword);
            this->Controls->Add(this->textboxUsername);
            this->Controls->Add(this->textboxServerPort);
            this->Controls->Add(this->labelPassword);
            this->Controls->Add(this->labelUsername);
            this->Controls->Add(this->labelServerPort);
            this->Controls->Add(this->labelServerIP);
            this->Controls->Add(this->textboxServerIP);
            this->Name = L"RegisterForm";
            this->Text = L"RegisterForm";
            this->ResumeLayout(false);
            this->PerformLayout();

        }
#pragma endregion

    

    private: Boolean textboxServerIP_Validate() {
        String^ error;
        IPAddress^ ip;

        bool validIP = IPAddress::TryParse(textboxServerIP->Text, ip);
        if (textboxServerIP->Text->Length == 0) {
            error = "Enter Server IP";
            errorProviderServerIP->SetError(this->textboxServerIP, error);
            return false;
        }
        else if (!validIP) {
            error = "IP is not valid";
            errorProviderServerIP->SetError(this->textboxServerIP, error);
            return false;
        }
        errorProviderServerIP->SetError(this->textboxServerIP, error);
        return true;
    }
    private: System::Void textboxServerIP_Validated(System::Object^  sender, System::EventArgs^  e) {
        errorProviderServerIP->SetError(this->textboxServerIP, String::Empty);
    }
    private: System::Void textboxServerIP_Validating(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
        if (!textboxServerIP_Validate())
            e->Cancel = true;
    }

    private: Boolean textboxServerPort_Validate() {
        String^ error;
        int port;

        bool validPort = int::TryParse(textboxServerPort->Text, port) && port > 0 && port < 65536;
        if (textboxServerPort->Text->Length == 0) {
            error = "Enter Server Port";
            return false;
        }
        else if (!validPort) {
            error = "Port is not valid";
            return false;
        }
        errorProviderServerPort->SetError(this->textboxServerPort, error);
        return true;
    }
    private: System::Void textboxServerPort_Validating(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
        if (!textboxServerPort_Validate())
            e->Cancel = true;
    }
    private: System::Void textboxServerPort_Validated(System::Object^  sender, System::EventArgs^  e) {
        errorProviderServerPort->SetError(this->textboxServerPort, String::Empty);
    }

    private: Boolean textboxUsername_Validate() {
        String^ error;
        String^ username = textboxUsername->Text;
        if (username->Length == 0) {
            error = "Enter Username";
            return false;
        }
        errorProviderUsername->SetError(this->textboxUsername, error);
        return true;
    }
    private: System::Void textboxUsername_Validated(System::Object^  sender, System::EventArgs^  e) {
        errorProviderUsername->SetError(this->textboxUsername, String::Empty);
    }
    private: System::Void textboxUsername_Validating(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
        if (!textboxUsername)
            e->Cancel = true;
    }

    private: Boolean textboxPassword_Validate() {
        String^ error;
        String^ password = textboxPassword->Text;
        if (password->Length == 0) {
            error = "Enter Password";
            return false;
        }
        errorProviderPassword->SetError(this->textboxPassword, error);
        return true;
    }
    private: System::Void textboxPassword_Validating(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
        if (!textboxPassword_Validate())
            e->Cancel = true;
    }
    private: System::Void textboxPassword_Validated(System::Object^  sender, System::EventArgs^  e) {
        errorProviderPassword->SetError(this->textboxUsername, String::Empty);
    }

    private: System::Void buttonRegister_Click(System::Object^  sender, System::EventArgs^  e) {
        if (!textboxServerIP_Validate())
            return;
        if (!textboxServerPort_Validate())
            return;
        if (!textboxUsername_Validate())
            return;
        if (!textboxPassword_Validate())
            return;

        IPEndPoint^ serverAddress;
        String^ username;
        String^ password;

        serverAddress = gcnew IPEndPoint(IPAddress::Parse(textboxServerIP->Text), int::Parse(textboxServerPort->Text));
        password = textboxPassword->Text;
        username = textboxUsername->Text;

        ServerConnection^ connection;
        MessageStream^ stream;
        Message^ message;
        Dictionary<String^, String^>^ headers;
        try {
            connection = gcnew ServerConnection(serverAddress);
            stream = MessageStream::GetMessageStream(connection);
            stream->Send(gcnew Message(Message::MessageType::Hello));
            message = stream->Receive();
            if (message == nullptr || message->GetType() != Message::MessageType::Hello) {
                MessageBox::Show("Cannot connect to server", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
                delete connection;
                return;
            }

            headers = gcnew Dictionary<String^, String^>();
            headers->Add("Username", username);
            headers->Add("Password", password);
            stream->Send(gcnew Message(Message::MessageType::Register, gcnew ByteData(0), headers));

            message = stream->Receive();
            if (!(bool)message->HeaderBoolValue("Status")) {
                MessageBox::Show(message->HeaderStringValue("Reason"), "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
                delete connection;
                return;
            }
        }
        catch (IO::IOException^) {
            MessageBox::Show("Connection error", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
            if (connection)
                delete connection;
            return;
        }
        catch (Sockets::SocketException^) {
            MessageBox::Show("Connection error", "Error", MessageBoxButtons::OK, MessageBoxIcon::Error);
            if (connection)
                delete connection;
            return;
        }
        this->Connection = connection;
        this->Close();

    }
    private: System::Void buttonBack_Click(System::Object^  sender, System::EventArgs^  e) {
        this->Close();
    }

    public:
        ServerConnection^ Connection = nullptr;
};
}
