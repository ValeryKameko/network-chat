#pragma once
#include "Message.h"
#include "ServerConnection.h"

namespace Client {

    using namespace System::Net::Sockets;
    using ByteData = array<Byte>;

    public ref class MessageStream
    {
    private:
        NetworkStream^ Stream;
        ServerConnection^ Connection;

        Void RejectHandler(ServerConnection^ connection)
        {
            this->Stream->Close();
        }
    public:
        MessageStream(ServerConnection^ connection, NetworkStream^ stream)
        {
            this->Stream = stream;
            this->Connection = connection;
            connection->OnReject += gcnew ServerConnection::RejectHandler(this, &MessageStream::RejectHandler);
        };

        ~MessageStream() {
            Stream->Close();
        }

    public:
        System::Void Send(Message^ message)
        {
            ByteData^ data = message->Serialize();
            try {
                Stream->Write(data, 0, data->Length);
            }
            catch (IO::IOException^ e) {
                Connection->Reject();
                throw e;
            }
        }

        Message^ Receive()
        {
            Message^ message = gcnew Message();

            int it = 0;
            Boolean valid = true;
            ByteData^ messageNameData;
            String^ messageName;
            try {
                valid = valid && ReceiveHeaderEntry(messageNameData);
                valid = valid && ParseMessageName(messageNameData, messageName);
                valid = valid && Message::MessageTypesByName->ContainsKey(messageName);
                if (valid)
                    message->SetType(Message::MessageTypesByName[messageName]);

                while (valid) {
                    ByteData^ headerEntryData;
                    valid = valid && ReceiveHeaderEntry(headerEntryData);
                    if (valid && CheckHeadersEnd(headerEntryData))
                        break;
                    String^ header;
                    String^ value;
                    valid = valid && ParseHeaderEntry(headerEntryData, header, value);
                    if (valid)
                        message->SetHeader(header, value);
                }
                Int64 dataLength = 0;
                if (valid) {
                    Int64^ tempLength = message->HeaderInt64Value("DataLength");
                    if (tempLength == nullptr)
                        valid = false;
                    dataLength = (Int64)tempLength;
                }
                valid = valid && dataLength >= 0 && dataLength < Message::MaxMessageSize;
                if (valid) {
                    ByteData^ messageData = gcnew ByteData((int)dataLength);
                    int offset = 0;
                    while (offset < dataLength) {
                        int delta = Stream->Read(messageData, offset, (int)dataLength - offset);
                        if (delta == 0)
                            return nullptr;
                        offset += delta;
                    }
                    message->SetData(messageData);
                }
                if (valid)
                    return message;
                else
                    return nullptr;
            }
            catch (IO::IOException^ e) {
                Connection->Reject();
                throw e;
            }

        }

        static MessageStream^ GetMessageStream(ServerConnection^ connection)
        {
            MessageStream^ messageStream = gcnew MessageStream(connection, gcnew NetworkStream(connection->GetSocket()));
            return messageStream;
        }


    private:

        Byte MessageStream::GetByte(int% it, ByteData^ data)
        {
            return data[it];
        }

        Boolean MessageStream::GetSpecificBytes(int% it, ByteData^ data, ByteData^ bytes)
        {
            Boolean valid = true;
            for (int i = 0; i < bytes->Length; i++)
                valid = valid && GetSpecificByte(it, data, bytes[i]);
            return valid;
        }

        Boolean MessageStream::GetSpecificByte(int% it, ByteData^ data, Byte c)
        {
            if (it == data->Length || GetByte(it, data) != c)
                return false;
            it++;
            return true;
        }

        Boolean MessageStream::GetByteOneOf(int% it, ByteData^ data, ByteData^ allowed, Byte% readByte)
        {
            if (it == data->Length)
                return false;
            readByte = GetByte(it, data);
            if (Array::IndexOf(allowed, readByte) == -1)
                return false;
            it++;
            return true;
        }

        Boolean MessageStream::GetByteNotOneOf(int% it, ByteData^ data, ByteData^ allowed, Byte% readByte)
        {
            if (it == data->Length)
                return false;
            readByte = GetByte(it, data);
            if (Array::IndexOf(allowed, readByte) != -1)
                return false;
            it++;
            return true;
        }

        Boolean MessageStream::ReceiveHeaderEntry(ByteData^% data)
        {
            List<Byte>^ tempData = gcnew List<Byte>();
            while (true) {
                int byte;
                byte = Stream->ReadByte();
                if (byte == -1)
                    return false;
                tempData->Add(byte);
                if (byte == '\n')
                    break;
            }
            data = tempData->ToArray();
            return true;
        }

        Boolean MessageStream::ParseMessageName(ByteData^ data, String^% messageName)
        {
            Boolean valid = true;
            int it = 0;
            Byte readByte;

            messageName = gcnew String("");
            while (GetByteOneOf(it, data, Encoding::ASCII->GetBytes(Message::MetaCharsAllowed), readByte))
                messageName += Encoding::ASCII->GetString(gcnew array<Byte>(1) { readByte }, 0, 1);

            valid = valid && GetSpecificBytes(it, data, Encoding::ASCII->GetBytes("\r\n"));
            valid = valid && it == data->Length;
            return valid;
        }

        Boolean MessageStream::ParseHeaderEntry(ByteData^ data, String^% header, String^% value)
        {
            header = gcnew String("");
            value = gcnew String("");
            int it = 0;
            Boolean valid = true;
            Byte readByte;

            while (GetByteOneOf(it, data, Encoding::ASCII->GetBytes(Message::MetaCharsAllowed), readByte))
                header += Encoding::ASCII->GetString(gcnew array<Byte>(1) { readByte }, 0, 1);;
            valid = valid && GetSpecificBytes(it, data, Encoding::ASCII->GetBytes(": "));
            while (GetByteNotOneOf(it, data, Encoding::ASCII->GetBytes("\r\n"), readByte))
                value += Encoding::ASCII->GetString(gcnew array<Byte>(1) { readByte }, 0, 1);;
            valid = valid && GetSpecificBytes(it, data, Encoding::ASCII->GetBytes("\r\n"));
            valid = valid && it == data->Length;
            return valid;
        }

        Boolean MessageStream::CheckHeadersEnd(ByteData^ data)
        {
            Boolean valid = true;
            int it = 0;
            valid = valid && GetSpecificBytes(it, data, Encoding::ASCII->GetBytes("\r\n"));
            valid = valid && it == data->Length;
            return valid;
        }
    };
}