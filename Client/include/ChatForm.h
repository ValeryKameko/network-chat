#pragma once

#include "ChatMessageView.h"
#include "ServerConnection.h"
#include "MessageStream.h"

namespace Client {

    using namespace System;
    using namespace System::ComponentModel;
    using namespace System::Collections;
    using namespace System::Collections::Generic;
    using namespace System::Windows::Forms;
    using namespace System::Data;
    using namespace System::Drawing;
    using namespace System::Threading::Tasks;
    using namespace System::Text;

    /// <summary>
    /// ������ ��� ChatForm
    /// </summary>

    public ref class ChatForm : public System::Windows::Forms::Form
    {
    private:
        ServerConnection^ Connection;
    public:
        ChatForm(ServerConnection^ connection)
        {
            InitializeComponent();

            SendQueue = gcnew Generic::Queue<Message^>();
            QueueSemaphore = gcnew Threading::Semaphore(0, Int32::MaxValue);
            CloseConnectionSemaphore = gcnew Threading::Semaphore(0, Int32::MaxValue);
            Sender = gcnew Threading::Thread(gcnew Threading::ThreadStart(this, &ChatForm::SendThreadProc));
            Receiver = gcnew Threading::Thread(gcnew Threading::ThreadStart(this, &ChatForm::ReceiveThreadProc));

            Connection = connection;
            Sender->Start();
            Receiver->Start();
        }
    protected:
        /// <summary>
        /// ���������� ��� ������������ �������.
        /// </summary>
        ~ChatForm()
        {
            if (components)
            {
                delete components;
            }
        }
    private: System::Windows::Forms::Panel^  panelLeft;
    private: System::Windows::Forms::Panel^  panelLeftBottom;
    private: System::Windows::Forms::Button^  buttonLogout;
    private: System::Windows::Forms::Panel^  panelRight;
    private: System::Windows::Forms::Panel^  panelRightBottom;
    private: System::Windows::Forms::Panel^  panelLeftBottomRight;
    private: System::Windows::Forms::RichTextBox^  richTextboxMessage;
    private: System::Windows::Forms::Panel^  panelLeftBottomLeft;
    private: System::Windows::Forms::Button^  buttonSend;
    private: System::Windows::Forms::FlowLayoutPanel^  flowLayoutMessagesContainer;
    private: System::Windows::Forms::Panel^  panelMessagesContainer;
    private: System::Windows::Forms::Panel^  panelMessageContainerBorder;
    private: System::Windows::Forms::Panel^  panelHack;


    private: Threading::Thread^ Sender;
    private: Threading::Thread^ Receiver;
    private: Generic::Queue<Message^>^ SendQueue;
    private: Threading::Semaphore^ QueueSemaphore;
    private: Threading::Semaphore^ CloseConnectionSemaphore;
    private: Boolean IsHostCloseConnection = false;
    private: Boolean CloseConnection = false;


    private: Void SendThreadProc() {
        MessageStream^ stream = MessageStream::GetMessageStream(Connection);
        try {
            while (true) {
                int result = Threading::WaitHandle::WaitAny(gcnew array<Threading::WaitHandle^>{ CloseConnectionSemaphore, QueueSemaphore });
                if (result == 0) {
                    Control::BeginInvoke(gcnew Action(this, &ChatForm::FinishChat));
                    return;
                }
                Threading::Monitor::Enter(SendQueue);
                Message^ message = SendQueue->Dequeue();
                Threading::Monitor::Exit(SendQueue);

                stream->Send(message);
            }
        }
        catch (Exception^) {
            HandleThreadError("Server close connection");
        }
        finally {
            delete stream;
        }
    }

    private: Void ReceiveThreadProc() {
        MessageStream^ stream = MessageStream::GetMessageStream(Connection);
        Message^ message;
        try {
            while (true) {
                message = stream->Receive();
                if (message == nullptr) {
                    HandleThreadError("Received corrupted message");
                    return;
                }
                if (message->GetType() == Message::MessageType::Message) {
                    Control::BeginInvoke(gcnew Action<Message^>(this, &ChatForm::AddMessage), message);
                    continue;
                }
                if (message->GetType() == Message::MessageType::Bye) {
                    Control::BeginInvoke(gcnew Action(this, &ChatForm::FinishChat));
                    return;
                }
            }
        }
        catch (Exception^) {
            HandleThreadError("Server close connection");
        }
        finally {
            delete stream;
        }
    }

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
            this->panelLeft = (gcnew System::Windows::Forms::Panel());
            this->panelLeftBottom = (gcnew System::Windows::Forms::Panel());
            this->buttonLogout = (gcnew System::Windows::Forms::Button());
            this->panelRight = (gcnew System::Windows::Forms::Panel());
            this->panelMessagesContainer = (gcnew System::Windows::Forms::Panel());
            this->panelMessageContainerBorder = (gcnew System::Windows::Forms::Panel());
            this->flowLayoutMessagesContainer = (gcnew System::Windows::Forms::FlowLayoutPanel());
            this->panelHack = (gcnew System::Windows::Forms::Panel());
            this->panelRightBottom = (gcnew System::Windows::Forms::Panel());
            this->panelLeftBottomRight = (gcnew System::Windows::Forms::Panel());
            this->richTextboxMessage = (gcnew System::Windows::Forms::RichTextBox());
            this->panelLeftBottomLeft = (gcnew System::Windows::Forms::Panel());
            this->buttonSend = (gcnew System::Windows::Forms::Button());
            this->panelLeft->SuspendLayout();
            this->panelLeftBottom->SuspendLayout();
            this->panelRight->SuspendLayout();
            this->panelMessagesContainer->SuspendLayout();
            this->panelMessageContainerBorder->SuspendLayout();
            this->flowLayoutMessagesContainer->SuspendLayout();
            this->panelRightBottom->SuspendLayout();
            this->panelLeftBottomRight->SuspendLayout();
            this->panelLeftBottomLeft->SuspendLayout();
            this->SuspendLayout();
            // 
            // panelLeft
            // 
            this->panelLeft->Controls->Add(this->panelLeftBottom);
            this->panelLeft->Dock = System::Windows::Forms::DockStyle::Right;
            this->panelLeft->Location = System::Drawing::Point(739, 0);
            this->panelLeft->Name = L"panelLeft";
            this->panelLeft->Size = System::Drawing::Size(118, 486);
            this->panelLeft->TabIndex = 0;
            // 
            // panelLeftBottom
            // 
            this->panelLeftBottom->Controls->Add(this->buttonLogout);
            this->panelLeftBottom->Dock = System::Windows::Forms::DockStyle::Bottom;
            this->panelLeftBottom->Location = System::Drawing::Point(0, 400);
            this->panelLeftBottom->Name = L"panelLeftBottom";
            this->panelLeftBottom->Padding = System::Windows::Forms::Padding(10);
            this->panelLeftBottom->Size = System::Drawing::Size(118, 86);
            this->panelLeftBottom->TabIndex = 1;
            // 
            // buttonLogout
            // 
            this->buttonLogout->Dock = System::Windows::Forms::DockStyle::Fill;
            this->buttonLogout->Location = System::Drawing::Point(10, 10);
            this->buttonLogout->Name = L"buttonLogout";
            this->buttonLogout->Size = System::Drawing::Size(98, 66);
            this->buttonLogout->TabIndex = 0;
            this->buttonLogout->Text = L"Logout";
            this->buttonLogout->UseVisualStyleBackColor = true;
            this->buttonLogout->Click += gcnew System::EventHandler(this, &ChatForm::buttonLogout_Click);
            // 
            // panelRight
            // 
            this->panelRight->Controls->Add(this->panelMessagesContainer);
            this->panelRight->Controls->Add(this->panelRightBottom);
            this->panelRight->Dock = System::Windows::Forms::DockStyle::Fill;
            this->panelRight->Location = System::Drawing::Point(0, 0);
            this->panelRight->Name = L"panelRight";
            this->panelRight->Size = System::Drawing::Size(739, 486);
            this->panelRight->TabIndex = 1;
            // 
            // panelMessagesContainer
            // 
            this->panelMessagesContainer->Controls->Add(this->panelMessageContainerBorder);
            this->panelMessagesContainer->Dock = System::Windows::Forms::DockStyle::Fill;
            this->panelMessagesContainer->Location = System::Drawing::Point(0, 0);
            this->panelMessagesContainer->Name = L"panelMessagesContainer";
            this->panelMessagesContainer->Padding = System::Windows::Forms::Padding(20);
            this->panelMessagesContainer->Size = System::Drawing::Size(739, 400);
            this->panelMessagesContainer->TabIndex = 2;
            // 
            // panelMessageContainerBorder
            // 
            this->panelMessageContainerBorder->BackColor = System::Drawing::Color::Black;
            this->panelMessageContainerBorder->Controls->Add(this->flowLayoutMessagesContainer);
            this->panelMessageContainerBorder->Dock = System::Windows::Forms::DockStyle::Fill;
            this->panelMessageContainerBorder->Location = System::Drawing::Point(20, 20);
            this->panelMessageContainerBorder->Name = L"panelMessageContainerBorder";
            this->panelMessageContainerBorder->Padding = System::Windows::Forms::Padding(3);
            this->panelMessageContainerBorder->Size = System::Drawing::Size(699, 360);
            this->panelMessageContainerBorder->TabIndex = 2;
            // 
            // flowLayoutMessagesContainer
            // 
            this->flowLayoutMessagesContainer->AutoScroll = true;
            this->flowLayoutMessagesContainer->BackColor = System::Drawing::SystemColors::Control;
            this->flowLayoutMessagesContainer->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
            this->flowLayoutMessagesContainer->Controls->Add(this->panelHack);
            this->flowLayoutMessagesContainer->Dock = System::Windows::Forms::DockStyle::Fill;
            this->flowLayoutMessagesContainer->FlowDirection = System::Windows::Forms::FlowDirection::TopDown;
            this->flowLayoutMessagesContainer->Location = System::Drawing::Point(3, 3);
            this->flowLayoutMessagesContainer->Name = L"flowLayoutMessagesContainer";
            this->flowLayoutMessagesContainer->Size = System::Drawing::Size(693, 354);
            this->flowLayoutMessagesContainer->TabIndex = 1;
            this->flowLayoutMessagesContainer->WrapContents = false;
            this->flowLayoutMessagesContainer->Layout += gcnew System::Windows::Forms::LayoutEventHandler(this, &ChatForm::flowLayoutMessagesContainer_Layout);
            // 
            // panelHack
            // 
            this->panelHack->Location = System::Drawing::Point(0, 0);
            this->panelHack->Margin = System::Windows::Forms::Padding(0);
            this->panelHack->Name = L"panelHack";
            this->panelHack->Size = System::Drawing::Size(672, 0);
            this->panelHack->TabIndex = 1;
            // 
            // panelRightBottom
            // 
            this->panelRightBottom->Controls->Add(this->panelLeftBottomRight);
            this->panelRightBottom->Controls->Add(this->panelLeftBottomLeft);
            this->panelRightBottom->Dock = System::Windows::Forms::DockStyle::Bottom;
            this->panelRightBottom->Location = System::Drawing::Point(0, 400);
            this->panelRightBottom->Name = L"panelRightBottom";
            this->panelRightBottom->Padding = System::Windows::Forms::Padding(10);
            this->panelRightBottom->Size = System::Drawing::Size(739, 86);
            this->panelRightBottom->TabIndex = 0;
            // 
            // panelLeftBottomRight
            // 
            this->panelLeftBottomRight->Controls->Add(this->richTextboxMessage);
            this->panelLeftBottomRight->Dock = System::Windows::Forms::DockStyle::Fill;
            this->panelLeftBottomRight->Location = System::Drawing::Point(10, 10);
            this->panelLeftBottomRight->Name = L"panelLeftBottomRight";
            this->panelLeftBottomRight->Padding = System::Windows::Forms::Padding(10);
            this->panelLeftBottomRight->Size = System::Drawing::Size(605, 66);
            this->panelLeftBottomRight->TabIndex = 3;
            // 
            // richTextboxMessage
            // 
            this->richTextboxMessage->Dock = System::Windows::Forms::DockStyle::Fill;
            this->richTextboxMessage->Location = System::Drawing::Point(10, 10);
            this->richTextboxMessage->Name = L"richTextboxMessage";
            this->richTextboxMessage->Size = System::Drawing::Size(585, 46);
            this->richTextboxMessage->TabIndex = 0;
            this->richTextboxMessage->Text = L"";
            // 
            // panelLeftBottomLeft
            // 
            this->panelLeftBottomLeft->Controls->Add(this->buttonSend);
            this->panelLeftBottomLeft->Dock = System::Windows::Forms::DockStyle::Right;
            this->panelLeftBottomLeft->Location = System::Drawing::Point(615, 10);
            this->panelLeftBottomLeft->Name = L"panelLeftBottomLeft";
            this->panelLeftBottomLeft->Padding = System::Windows::Forms::Padding(10);
            this->panelLeftBottomLeft->Size = System::Drawing::Size(114, 66);
            this->panelLeftBottomLeft->TabIndex = 2;
            // 
            // buttonSend
            // 
            this->buttonSend->Dock = System::Windows::Forms::DockStyle::Fill;
            this->buttonSend->Location = System::Drawing::Point(10, 10);
            this->buttonSend->Name = L"buttonSend";
            this->buttonSend->Size = System::Drawing::Size(94, 46);
            this->buttonSend->TabIndex = 1;
            this->buttonSend->Text = L"Send";
            this->buttonSend->UseVisualStyleBackColor = true;
            this->buttonSend->Click += gcnew System::EventHandler(this, &ChatForm::buttonSend_Click);
            // 
            // ChatForm
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->ClientSize = System::Drawing::Size(857, 486);
            this->Controls->Add(this->panelRight);
            this->Controls->Add(this->panelLeft);
            this->Name = L"ChatForm";
            this->Text = L"ChatForm";
            this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &ChatForm::ChatForm_FormClosed);
            this->panelLeft->ResumeLayout(false);
            this->panelLeftBottom->ResumeLayout(false);
            this->panelRight->ResumeLayout(false);
            this->panelMessagesContainer->ResumeLayout(false);
            this->panelMessageContainerBorder->ResumeLayout(false);
            this->flowLayoutMessagesContainer->ResumeLayout(false);
            this->panelRightBottom->ResumeLayout(false);
            this->panelLeftBottomRight->ResumeLayout(false);
            this->panelLeftBottomLeft->ResumeLayout(false);
            this->ResumeLayout(false);

        }
#pragma endregion
    private: System::Void flowLayoutMessagesContainer_Layout(System::Object^  sender, System::Windows::Forms::LayoutEventArgs^  e) {
        this->panelHack->Width = this->flowLayoutMessagesContainer->ClientSize.Width - flowLayoutMessagesContainer->Padding.Horizontal;
    }
    
    private: Void ShowThreadError(String^ errorMessage) {
        MessageBox::Show(errorMessage, "Error", MessageBoxButtons::OK,  MessageBoxIcon::Error);
    }

    private: Void HandleThreadError(String^ errorMessage) {
        Control::BeginInvoke(gcnew Action<String^>(this, &ChatForm::ShowThreadError), errorMessage);
        Control::BeginInvoke(gcnew Action(this, &ChatForm::FinishChat));
    }

    private: Void FinishChat() {
        this->CloseConnectionSemaphore->Release();
        if (!CloseConnection) {
            delete Connection;
            CloseConnection = true;
            Sender->Join();
            Receiver->Join();
            this->Close();
        }
    }

    private: Void AddMessage(Message^ message) {
        String^ username = message->HeaderStringValue("Author");
        String^ data = Encoding::ASCII->GetString(message->GetData());
        Boolean^ isOwner = message->HeaderBoolValue("Owner");
        this->flowLayoutMessagesContainer->Controls->Add(gcnew ChatMessageView(username, data, (Boolean)isOwner));
    }

    private: System::Void buttonLogout_Click(System::Object^  sender, System::EventArgs^  e) {
        this->Close();
    }
    private: System::Void ChatForm_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
        FinishChat();
    }
    private: System::Void buttonSend_Click(System::Object^  sender, System::EventArgs^  e) {
        ByteData^ data = Encoding::ASCII->GetBytes(richTextboxMessage->Text);
        Dictionary<String^, String^>^ headers = gcnew Dictionary<String^, String^>();
        Message^ message = gcnew Message(Message::MessageType::Message, data, headers);

        Threading::Monitor::Enter(SendQueue);
        SendQueue->Enqueue(message);
        QueueSemaphore->Release();
        Threading::Monitor::Exit(SendQueue);
    }
};
}
