#pragma once

namespace Client {

    using namespace System;
    using namespace System::Net;
    using namespace System::Net::Sockets;

    ref class MessageStream;

    public ref class ServerConnection
    {
    private:
        Boolean IsRejected;
        Socket^ server;
    public:
        delegate Void RejectHandler(ServerConnection^ connection);
        event RejectHandler^ OnReject;

        ServerConnection(IPEndPoint^ address)
        {
            IsRejected = false;
            server = gcnew Socket(AddressFamily::InterNetwork, SocketType::Stream, ProtocolType::Tcp);
            server->Connect(address);
        }

        ~ServerConnection()
        {
            Reject();
        }

        Socket^ GetSocket()
        {
            return server;
        }

        Void Reject()
        {
            if (!IsRejected) {
                IsRejected = true;
                OnReject(this);
                if (server) {
                    server->Disconnect(false);
                    delete server;
                }
            }
        }
    };
}
