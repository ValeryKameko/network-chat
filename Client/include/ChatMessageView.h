#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace Client {

	/// <summary>
	/// ������ ��� ChatMessageView
	/// </summary>
	public ref class ChatMessageView : public System::Windows::Forms::UserControl
	{
	public:
		ChatMessageView(String^ author, String^ message, Boolean isHostMessage)
		{
			InitializeComponent();
            textBoxAuthor->Text = author;
            textBoxMessage->Text = message;

            this->Dock = isHostMessage ? DockStyle::Right : DockStyle::Left;
            this->BackColor = isHostMessage ? Color::LightBlue : Color::LightGray;

            textBoxAuthor->Size = TextRenderer::MeasureText(textBoxAuthor->Text, textBoxAuthor->Font);
            textBoxMessage->Size = TextRenderer::MeasureText(textBoxMessage->Text, textBoxMessage->Font, textBoxMessage->Size, TextFormatFlags::WordBreak);
            textBoxAuthor->BackColor = this->BackColor;
            textBoxMessage->BackColor = this->BackColor;
            this->PerformLayout();
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~ChatMessageView()
		{
			if (components)
			{
				delete components;
			}
		}

    private: System::Windows::Forms::Panel^  panelTop;
    private: System::Windows::Forms::Panel^  panelBottom;
    private: System::Windows::Forms::TextBox^  textBoxAuthor;
    private: System::Windows::Forms::TextBox^  textBoxMessage;
    private: System::Windows::Forms::Panel^  panelBorder;


	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
            this->panelTop = (gcnew System::Windows::Forms::Panel());
            this->textBoxAuthor = (gcnew System::Windows::Forms::TextBox());
            this->panelBottom = (gcnew System::Windows::Forms::Panel());
            this->textBoxMessage = (gcnew System::Windows::Forms::TextBox());
            this->panelBorder = (gcnew System::Windows::Forms::Panel());
            this->panelTop->SuspendLayout();
            this->panelBottom->SuspendLayout();
            this->panelBorder->SuspendLayout();
            this->SuspendLayout();
            // 
            // panelTop
            // 
            this->panelTop->Controls->Add(this->textBoxAuthor);
            this->panelTop->Dock = System::Windows::Forms::DockStyle::Top;
            this->panelTop->Location = System::Drawing::Point(5, 5);
            this->panelTop->MinimumSize = System::Drawing::Size(0, 20);
            this->panelTop->Name = L"panelTop";
            this->panelTop->Padding = System::Windows::Forms::Padding(5);
            this->panelTop->Size = System::Drawing::Size(295, 26);
            this->panelTop->TabIndex = 0;
            this->panelTop->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &ChatMessageView::panel1_Paint);
            // 
            // textBoxAuthor
            // 
            this->textBoxAuthor->BorderStyle = System::Windows::Forms::BorderStyle::None;
            this->textBoxAuthor->Dock = System::Windows::Forms::DockStyle::Right;
            this->textBoxAuthor->Location = System::Drawing::Point(116, 5);
            this->textBoxAuthor->Multiline = true;
            this->textBoxAuthor->Name = L"textBoxAuthor";
            this->textBoxAuthor->ReadOnly = true;
            this->textBoxAuthor->Size = System::Drawing::Size(174, 16);
            this->textBoxAuthor->TabIndex = 1;
            // 
            // panelBottom
            // 
            this->panelBottom->Controls->Add(this->textBoxMessage);
            this->panelBottom->Dock = System::Windows::Forms::DockStyle::Fill;
            this->panelBottom->Location = System::Drawing::Point(0, 3);
            this->panelBottom->Margin = System::Windows::Forms::Padding(0);
            this->panelBottom->Name = L"panelBottom";
            this->panelBottom->Size = System::Drawing::Size(295, 72);
            this->panelBottom->TabIndex = 1;
            this->panelBottom->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &ChatMessageView::panel2_Paint);
            // 
            // textBoxMessage
            // 
            this->textBoxMessage->BorderStyle = System::Windows::Forms::BorderStyle::None;
            this->textBoxMessage->Dock = System::Windows::Forms::DockStyle::Fill;
            this->textBoxMessage->Location = System::Drawing::Point(0, 0);
            this->textBoxMessage->Multiline = true;
            this->textBoxMessage->Name = L"textBoxMessage";
            this->textBoxMessage->ReadOnly = true;
            this->textBoxMessage->Size = System::Drawing::Size(295, 72);
            this->textBoxMessage->TabIndex = 0;
            // 
            // panelBorder
            // 
            this->panelBorder->BackColor = System::Drawing::Color::Black;
            this->panelBorder->Controls->Add(this->panelBottom);
            this->panelBorder->Dock = System::Windows::Forms::DockStyle::Fill;
            this->panelBorder->Location = System::Drawing::Point(5, 31);
            this->panelBorder->Margin = System::Windows::Forms::Padding(0);
            this->panelBorder->Name = L"panelBorder";
            this->panelBorder->Padding = System::Windows::Forms::Padding(0, 3, 0, 0);
            this->panelBorder->Size = System::Drawing::Size(295, 75);
            this->panelBorder->TabIndex = 1;
            // 
            // ChatMessageView
            // 
            this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
            this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
            this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
            this->Controls->Add(this->panelBorder);
            this->Controls->Add(this->panelTop);
            this->MinimumSize = System::Drawing::Size(200, 60);
            this->Name = L"ChatMessageView";
            this->Padding = System::Windows::Forms::Padding(5);
            this->Size = System::Drawing::Size(305, 111);
            this->Load += gcnew System::EventHandler(this, &ChatMessageView::ChatMessageView_Load);
            this->panelTop->ResumeLayout(false);
            this->panelTop->PerformLayout();
            this->panelBottom->ResumeLayout(false);
            this->panelBottom->PerformLayout();
            this->panelBorder->ResumeLayout(false);
            this->ResumeLayout(false);

        }
#pragma endregion
    private: System::Void panel2_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
    }
    private: System::Void panel1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
    }
    private: System::Void ChatMessageView_Load(System::Object^  sender, System::EventArgs^  e) {
    }
};
}
